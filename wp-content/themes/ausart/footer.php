
<?php
global $themeple_config;

wp_reset_postdata();
$social_icons = themeple_get_option('social_icons');


?>
 
 <!-- Social Profiles -->
    

<!-- Footer -->
    </div>


    <div class="footer_wrapper">
	
    
    <?php if(themeple_get_option('footer_social_bar') == 'yes'): ?>
        <div class="footer_social_bar">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                       <div class="top_footer"><div class="container"><div class="title"><i class="moon-twitter"></i><span><?php _e('Some Recent Tweets', 'themeple'); ?></span></div><div class="triangle"></div><?php echo themeple_get_twitter_top_footer(3, themeple_get_option('twitter_account')) ?><div class="pagination pull-right"><a href="#" class="back"><i class="moon-arrow-left"></i></a><a href="#" class="next"><i class="moon-arrow-right-2"></i></a></div><span class="shadow_top_footer"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <footer id="footer" class="type_<?php echo esc_attr(themeple_get_option('footer_skin') ) ?>">
        

    	<div class="inner">
	    	<div class="container">
	        	<div class="row-fluid ff add-border">
                
                	<!-- widget -->
		        	<?php
                    
                    $columns = esc_attr(themeple_get_option('footer_number_columns') );
                    for($i = 1; $i <= $columns; $i++){
                        ?>
                        <div class="span<?php echo 12/$columns ?>">
                        
                            <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer - column'.$i) ) : else : echo "<div class='widget'>Add Widget Column $i</div>"; endif; ?>
                            
                        </div>
                        
                        
                        
                         
                        <?php
                    }
                
                
                
                ?>
                    
	            </div>
	        </div>
<!--            <div class="arrow_down"><i class="icon-angle-up"></i></div>-->
        </div>
        
        <div id="copyright">
	    	<div class="container">
	        	<div class="row-fluid">
		        	<div class="span12 desc">
                    
                        <div class="span6">
                            <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Copyright Footer Sidebar Left') ) : else : echo "<div class='widget'>Add Widget Column</div>"; endif; ?>
                        </div>
                        <div class="span6 right-copyright"><?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Copyright Footer Sidebar Right') ) : else : echo "<div class='widget'>Add Widget Column</div>"; endif; ?></div>
                    
                    
                    </div>
                    
                </div>
            </div>
        </div><!-- #copyright -->
        <div>
            <?php //if (function_exists('dynamic_sidebar') && dynamic_sidebar('Bottom Footer Sidebar') ) : else : echo "<div class='widget'>Add Widget Column</div>"; endif; ?>
        </div>

    </footer><!-- #footer -->
</div>
<script>
    document.querySelector('.footer_social_icons .facebook a').href = document.querySelector('.footer_social_icons .facebook a').href.replace("httpsss","https");
    document.querySelector('.footer_social_icons .facebook a i').className = "fa fa-facebook";
    
    document.querySelector('.footer_social_icons .linkedin a').href = document.querySelector('.footer_social_icons .linkedin a').href.replace("httpsss","https");
    document.querySelector('.footer_social_icons .linkedin a i').className = "fa fa-linkedin";
    
    document.querySelector('.footer_social_icons .twitter a').href = document.querySelector('.footer_social_icons .twitter a').href.replace("httpsss","https");
    document.querySelector('.footer_social_icons .twitter a i').className = "fa fa-twitter";
    
    document.querySelector('.wpb_row').style.paddingTop = "2rem!important";
    document.querySelector('.wpb_row').style.paddingBottom = "2rem!important";
    
    var foot = document.querySelector('footer#footer .inner .row-fluid.ff.add-border');
    console.log(foot);
    var span = document.createElement("div");
    foot.insertBefore(span, foot.childNodes[0]);
    span.className = "span-big";
    span.innerHTML = "<div id='footer-image'><img src='https://www.zuman.com/wp-content/uploads/2018/11/HRO-White-Transparant.png'><div>";
    
    
    var correct = document.querySelectorAll('a');
    for(var i = 0; i < correct.length; i++){
        correct[i].href = correct[i].href.replace("httpsss","https");
        correct[i].href = correct[i].href.replace("httpss", "https");
    }
    
</script>
<script>
    function msieversion() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
            return true;
        else                 // If another browser, return 0
            return false;
    }
    //FIX problems with set same height for .parent-col-bg-white blocks
    if(msieversion()){
        jQuery(function($) {
            var set_height_parent_col_bg_white = function(recalculate){

                if($('.parent-col-bg-white').length) {
                    var elem_display_none = [];
                    if(recalculate == 1 && !first_exec ) {

                        //When window resize, we need delete old height and set new value
                        $('.parent-col-bg-white .column_container > .wpb_wrapper').each(function(){
                            $(this).find('.wpb_wrapper').css('height','auto');
                        });
                    }
                    $('.parent-col-bg-white .column_container > .wpb_wrapper').each(function(){

                        if($(this).parent().parent().parent().parent().css('display') == 'none') {
                            var item =  $(this).parent().parent().parent().parent();
                            item.css('display','block');
                            elem_display_none.push( item );
                        }

                        if( $(this).parent().css('display') == "table-cell" ) {
                            var height = $(this).parent().height();
                            $(this).find('.wpb_wrapper').css('height',height+"px");
                        }

                    });
                    while(elem_display_none.length){
                        var item = elem_display_none.pop();
                        item.css('display','none');
                    }
                }
                first_exec = false;
            }
            var first_exec = true;
            set_height_parent_col_bg_white(0);

            $(window).resize(function(){
                set_height_parent_col_bg_white(1);
            });
        });
    }


</script>
<?php $layout = themeple_get_option('overall_layout'); if($layout == 'boxed'): ?>
</div>

<?php endif; ?>
<?php
wp_footer();
?>
</body>
</html>