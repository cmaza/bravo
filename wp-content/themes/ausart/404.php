<?php

get_header();

?>

<!-- Page Head -->
  
 


   
	    <section id="content" style="padding-top:0px !important; padding-bottom:0px !important;">

   

	    	<div class="row-fluid row-dynamic-el" style=" margin-bottom:100px;">

                 <div class="container">

                      <div class="row-fluid">

                         <div class="row-fluid row-dynamic-el " style="">

                                 <div class="container">

                                       <div class="row-fluid">
                                                
                                          <div class="span12 dynamic_page_header not_found_error">

                                                    <div class="headings">
                                                          <h2>We're sorry, but the page you are looking for cannot be found or has been removed.</h2>
                                                    </div>   

                                                    <div class="image_not_found">
                                                          <img src="/wp-content/uploads/2015/05/404_cta_button.png" />
                                                    </div>
                                                    <div class="search">
                                                            <form role="search" method="get" id="searchform" action="<?php echo esc_url(home_url( '/' )); ?>">
                                                                  <div>
                                                                      <input type="text" value="" name="s" id="s" />
                                                                      <input type="submit" id="searchsubmit" value="Search" />
                                                                  </div>
                                                            </form>
                                                    </div>

                                                   
                                         </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                 </div>
               </div>

	    </section>
	
<?php
get_footer();


?>