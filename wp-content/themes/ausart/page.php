<?php
global $themeple_config;

    do_action( 'themeple_routing_template' , 'page' );
    $themeple_config['current_view'] = 'page';
    $meta = themeple_post_meta(themeple_get_post_id());


    if(!isset($themeple_config['current_sidebar']) ){
        $themeple_config['current_sidebar'] = 'fullsize';
    }

    if(isset($meta['layout']))
        $themeple_config['current_sidebar'] = $meta['layout'];
    $spancontent = 12;
    if(isset($themeple_config['current_sidebar']) && $themeple_config['current_sidebar'] == 'fullsize')
        $spancontent = 12;
    elseif(isset($themeple_config['current_sidebar']) && ($themeple_config['current_sidebar'] == 'sidebar_left' || $themeple_config['current_sidebar'] == 'sidebar_right'))
        $spancontent = 9;

    get_header();


    ?>

        <?php

            $title = get_the_title();
            $page_parents = themeple_page_parents();
            $subtitle = themeple_post_meta(themeple_get_post_id(), 'page_header_desc');
        ?>

    <?php get_template_part('template_inc/page_header'); ?>

   <section id="content"  <?php if(themeple_post_meta(themeple_get_post_id(), 'page_header_bool') == 'no'): echo 'style=""'; endif; ?> class="top_wrapper page_name_<?php echo $post->post_name; ?>">

       <?php
       /*    HERO IMAGE */
       if(themeple_post_meta(themeple_get_post_id(), 'page_header_bool') == 'yes') {
           $header_style = themeple_post_meta(themeple_get_post_id(), 'page_header_style');
           if(in_array($header_style ,
                            array("with_3_circules"
                                , "only_image"
                                , "main")
           )) {
                include(get_template_directory().'/page_headers_custom/header_'.$header_style.'.php');
           }
       }
       /*  END HERO IMAGE */
       ?>

<?php /* TOP MENU if activate */
if(!empty($activate_top_menu)): ?>
    <div class="page_sub_wrapper">
    <nav class="page_submenu">
    <ul>
        <?php
        ob_start();
        //hardcode page our-solutions
        $tmp_title = get_the_title($post->post_parent);
        $tmp_url = get_permalink($post->post_parent);
        if(preg_match('/our-solution/',$tmp_url)) {
            $tmp_title = "Overview";
        }
        ?>
        <li <?php if(is_page($post->post_parent)): ?>class="current_page_item"<?php endif; ?>><a href="<?php echo esc_url($tmp_url); ?>" ><?php echo $tmp_title; ?></a></li>

        <?php
        if($post->post_parent) {
            $children = wp_list_pages("title_li=&child_of=".esc_attr(themeple_get_post_top_ancestor_id())."&echo=0");

        }else{
            $children = wp_list_pages("title_li=&child_of=".esc_attr($post->ID)."&echo=0");

        }
        if ($children) { ?>



        <?php echo $children; ?>


        <?php }  ?>
        <?php $output = ob_get_contents();
        ob_end_clean();
        $url = get_permalink();
        $output = str_replace($url.'"', $url.'" class="active"', $output);
        echo $output;
        ?>
    </ul>
    </nav>
    </div>
<?php endif; ?>

            <?php
            //Hask for add active class to link with ative url
            ob_start();
                get_template_part( 'template_inc/loop', 'page' );
                wp_reset_postdata();
            $output = ob_get_contents();
            ob_end_clean();

            if(strlen($_SERVER['REQUEST_URI']) > 3) {
                $output = str_replace ($_SERVER['REQUEST_URI'].'"', $_SERVER['REQUEST_URI'].'" class="active"', $output);
            }
            echo $output;
            ?>

        <?php /* <?php } ?> */ ?>

</section>

    <?php
    get_footer();

?>