<?php
$hero_height = (themeple_post_meta(themeple_get_post_id(), 'hero_height'));
$hero_bg = (themeple_post_meta(themeple_get_post_id(), 'background_image'));
$hero_title = (themeple_post_meta(themeple_get_post_id(), 'hero_title'));

$add_style[] = "";
$add_margin_circules[] = "";
if(!empty($hero_height)) {
    $add_style[] = 'height:'.$hero_height.';';
}
if(!empty($hero_bg)) {
    
    if(strpos($hero_bg, 'httpsss') === false){
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }else{ 
        $hero_bg = str_replace('httpsss','https',$hero_bg);
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }
    
    if(strpos($hero_bg, 'httpss') === false){
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }else{ 
        $hero_bg = str_replace('httpss','https',$hero_bg);
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }
    
}
if(!empty($hero_title)) {
    $hero_title = '<h1 class="hero_header_text">'.$hero_title.'</h1>';
}
?>
<div style="background: #fff; overflow:hidden;">
    <div class="hero_bg" style="<?php echo implode(' ',$add_style); ?>" >
    </div>
<div class="bg-orange">
        <div class="container">
            <?php echo $hero_title; ?>
        </div>
    </div>
</div>