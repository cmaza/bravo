<?php
$hero_height = (themeple_post_meta(themeple_get_post_id(), 'hero_height'));
$hero_bg = (themeple_post_meta(themeple_get_post_id(), 'background_image'));
$hero_title = (themeple_post_meta(themeple_get_post_id(), 'hero_title'));
$hero_center = (themeple_post_meta(themeple_get_post_id(), 'hero_main'));

$add_style[] = "";
$add_margin_circules[] = "";
$css_height = "";
if(!empty($hero_height)) {
    $add_style[] = 'height:'.$hero_height.';';
    $css_height = 'height:'.$hero_height.';';
}
if(!empty($hero_bg)) {
    
    if(strpos($hero_bg, 'httpsss') === false){
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }else{ 
        $hero_bg = str_replace('httpsss','https',$hero_bg);
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }
    
    if(strpos($hero_bg, 'httpss') === false){
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }else{ 
        $hero_bg = str_replace('httpss','https',$hero_bg);
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }
    
}
if(!empty($hero_center)) {
    $hero_center = '<div class="main_circule_text" style="text-align:center;width:400px;display:block; margin:0 auto;">'.$hero_center.'</div>';
}
if(!empty($hero_title)) {
    $hero_title = '<h1 class="hero_header_text">'.$hero_title.'</h1>';
}
?>
<div style="background: #fff; overflow:hidden;">

    <div class="hero_bg" style="width:100%; overflow:hidden;<?php echo implode(' ',$add_style); ?>" >
        <svg id="hero_main_bg" style="<?php echo $css_height; ?>" viewBox="0 0 2800 400">
            <path d="M0,0 L2800,0 L2800,400 L0,400 L0,0 Z M1400,383 C1501.06811,383 1583,301.068109 1583,200 C1583,98.9318908 1501.06811,17 1400,17 C1298.93189,17 1217,98.9318908 1217,200 C1217,301.068109 1298.93189,383 1400,383 Z" fill="#ffffff" opacity="1"></path>
        </svg>
        <svg id="hero_main_circule"  style="<?php echo $css_height; ?>" viewBox="0 0 2800 400">
            <g id="circule">
                <circle cx="1400" cy="200" r="182" stroke="#ffffff" fill="transparent" stroke-width="2"></circle>
                <circle cx="1400" cy="200" r="179" fill="#ffffff"></circle>
            </g>
        </svg>
        <div id="hero_main_text" style=" <?php echo $css_height; ?> ">
            <div class="hero_main_text_inner">
                <?php echo $hero_center; ?>
            </div>
        </div>
    </div>
<div class="bg-orange">
    <div class="container">
        <?php echo $hero_title; ?>
    </div>
</div>
    

</div>
<script>
    jQuery(function($) {
        setTimeout( function(){
        $('#hero_main_bg').animate({"opacity":0}, 5000);
        $('#hero_main_circule').animate({"opacity":1}, 1300);
        $('#hero_main_text').animate({"opacity":1}, 2000);
        }, 700);
    });
</script>