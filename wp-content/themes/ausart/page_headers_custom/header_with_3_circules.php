<?php
$hero_height = (themeple_post_meta(themeple_get_post_id(), 'hero_height'));
$hero_bg = (themeple_post_meta(themeple_get_post_id(), 'background_image'));
$hero_title = (themeple_post_meta(themeple_get_post_id(), 'hero_title'));

$add_style[] = "";
$add_margin_circules[] = "";
if(!empty($hero_height)) {
    $add_style[] = 'height:'.$hero_height.';';
    //calculate margin top for circules
    if(intval($hero_height)>0) {
       // $add_margin_circules[] = 'padding-top:'.((intval($hero_height)-223)/2).'px;';
    }
}
if(!empty($hero_bg)) {
    
    if(strpos($hero_bg, 'httpsss') === false){
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }else{ 
        $hero_bg = str_replace('httpsss','https',$hero_bg);
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }
    
    if(strpos($hero_bg, 'httpss') === false){
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }else{ 
        $hero_bg = str_replace('httpss','https',$hero_bg);
        $add_style[] = 'background-image:url('.$hero_bg.');';
    }
    
}
if(!empty($hero_title)) {
    $hero_title = '<div class="hero_title_wrapper"><h1 class="hero_header_text">'.$hero_title.'</h1></div>';
}
?>
<div class="hero-block">
    <div class="bg-orange">
        <div class="container">
            <?php echo $hero_title; ?>
        </div>
    </div>
    <div class="hero_bg" style="<?php echo implode(' ',$add_style); ?>" >
            <div class="hero_circules group_big_circules white-color" style="<?php echo implode(' ',$add_margin_circules); ?>" >

                <div class="big-circul-border">
                <div class="big-circul">
                <div class="cirlabel">
                    <div class="cirlabel-helper">
                         <?php echo themeple_post_meta(themeple_get_post_id(), 'hero_first'); ?>
                    </div>
                </div>
                </div>
                </div>

                <div class="big-circul-border">
                <div class="big-circul">
                <div class="cirlabel">
                    <div class="cirlabel-helper">
                        <?php echo themeple_post_meta(themeple_get_post_id(), 'hero_second'); ?>
                    </div>
                </div>
                </div>
                </div>

                <div class="big-circul-border">
                <div class="big-circul">
                <div class="cirlabel">
                    <div class="cirlabel-helper">
                        <?php echo themeple_post_meta(themeple_get_post_id(), 'hero_third'); ?>
                    </div>
                </div>
                </div>
                </div>

        </div>
    </div>

</div>