<?php



		extract(shortcode_atts(array(

            
            'title' => '',
            'testimon' => '',
            'style' => ''




    	), $atts));

		$output = ''; 

         if($style == 'normal'){

            $output .= '<div class="header"><h2>'.$title.'</h2></div>';

        }

        $output .= '<div class="wpb_content_element custom_single_testimonial_wrapper tst_wrap '.$style.'">';

        

        if(!isset($testimon))

        $testimon = 0;          

        $query_post = array('posts_per_page'=> -1, 'post_type'=> 'testimonial', "order"=>'ASC');




                $loop = new WP_Query($query_post);

                             if($loop->have_posts()){



                                while($loop->have_posts()){



                                    $loop->the_post();

                                    $output .= '<div class="row custom_single_testimonial">';
                                    $output .= '<div class="vc_col-sm-4 wpb_column column_container">
                                                    <div class="wpb_wrapper">';


                                        $output .= '<div class="tst_header">
                                                <div class="tst_userpic">'.get_the_post_thumbnail($post->ID,  array(60,60)).'</div>';
                                        $output .= '<div class="tst_position"> '.themeple_post_meta($post->ID, 'staff_position_').'</div>
                                        </div>';
                                    $output .= '</div>'; //wpb_wrapper
                                    $output .= '</div>'; //vc_col-sm-4

                                    $output .= '<div class="vc_col-sm-8 wpb_column column_container">
                                                    <div class="wpb_wrapper">';
                                        $output .= '<div class="tst_content">'.get_the_content().'</div>';
                                    $output .= '</div>'; //wpb_wrapper
                                    $output .= '</div>'; //vc_col-sm-4

                                    $output .= '</div>';


                                }

                            }

      
        


        wp_reset_postdata();
        $output .= '</div>';



        $output = '<div class="wpb_row vc_row-fluid" >
            <div class="container  dark">
                <div class="section_clear">'
                    . $output .
                '</div>
            </div>
        </div>';
        echo $output;

?>